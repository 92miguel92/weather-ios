//
//  ViewController.swift
//  weather
//
//  Created by Antonio Pertusa on 18/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, XMLParserDelegate, ConnectionDelegate {
    
    @IBOutlet weak var cityName: UITextField!
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    
    @IBOutlet weak var icon: UIImageView!
    
    // Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cityName.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.cityName.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func floatToString(_ value: Float?) -> String
    {
        if let value = value {
            return "\(value)"
        }
        return "--"
    }
    
    func updateView(weather : Weather)
    {
        self.humidity.text = self.floatToString(weather.humidity)
        self.temperature.text = self.floatToString(weather.temperature)
        self.windSpeed.text = self.floatToString(weather.windSpeed)
        self.weatherDescription.text = weather.description
        self.country.text = weather.country
        
        // Descargar y actualizar la imagen aquí
        
        let url = URL(string:weather.imageURL!)
        let request = URLRequest(url:url!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        session.dataTask( with:request, completionHandler: { data, response, error in
            if error == nil, let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                DispatchQueue.main.async {
                    self.icon.image = UIImage(data: data)
                }
            }
        }).resume()
    }


    func parseJSON(_ data: Data) {
        // Guardar data como un Dictionary
        // Hacer algo con la variable json
        // Mostrarlo por consola con print. () significa array, {} es diccionario
        // Llamar al constructor de weather para extraer la información
        // Contenido JSON obtenido de la red, por ejemplo
        if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any] {
            print(jsonData)
            let w = Weather(json:jsonData)
            // Llamar a updateView para actualizar las etiquetas correspondientes
            updateView(weather: w!)
        }

    }
    
    func parseXML(_ data: Data) {
        // Inicializar el parser. La extracción se hará en los métodos delegados de XMLParserDelegate
        // En este caso, se creará un objeto de clase Weather y se escribirá en sus variables públicas conforme se va leyendo el XML, como puede verse abajo.
        let parser = XMLParser(data:data)
        parser.delegate = self
        parser.parse()
    }
    
    
    @IBAction func search(_ sender: Any) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let type = self.segmentedControl.titleForSegment(at: self.segmentedControl.selectedSegmentIndex)
        
        let apiKey = "ba01089290fe01d324a7bd5dcfb5a603"
        let urlString : String?
        
        if type == "JSON" {
            urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(self.cityName.text!)&APPID=\(apiKey)"
        }
        else {
            urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(self.cityName.text!)&mode=XML&APPID=\(apiKey)"
        }

        createConnection(urlString: urlString!)
    }
    
    func createConnection(urlString : String) {
        // Creamos la conexión
        let connection = Connection(delegate: self)
        // Lanzamos una petición
        connection.startConnection(URLSession.shared, with: urlString)
    }
    
    func connectionSucceed(_ connection: Connection, with data: Data) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        // Para debugging
        if let contents = String(data: data, encoding : .utf8) {
            print ("Received: \(contents)")
        }
        
        if self.segmentedControl.titleForSegment(at: self.segmentedControl.selectedSegmentIndex) == "XML" {
            self.parseXML(data)
        }
        else {
            self.parseJSON(data)
        }
        
        // URL de ejemplo para un icono: http://openweathermap.org/img/w/10d.png
    }
    
    func connectionFailed(_ connection: Connection, with error: String) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        print(error)
    }
    
    // XML Parsing
    private var currentMessage : String?
    private var weather = Weather()!
    
    func parserDidStartDocument(_ parser: XMLParser) {
        self.weather = Weather()! // Creamos un objeto weather con valores por defecto
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        self.updateView(weather: self.weather) // Actualizamos la vista cuando el parser haya terminado
    }

    // Falta añadir aquí los métodos delegados didStartElement, didEndElement y foundCharacters
    func parser(_ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName: String?,
                attributes attributeDict: [String : String] = [:])
    {
        if elementName.lowercased() == "current" {
            // Ok, no hacer nada
        }
        else if elementName.lowercased() == "city" {
            // Ok, no hacer nada
        }
        else if elementName.lowercased() == "coord" {
            // Ok, no hacer nada
        }
        else if elementName.lowercased() == "country" {
            
        }
        else if elementName.lowercased() == "sun" {
            // Ok, no hacer nada
        }
        else if elementName.lowercased() == "temperature" {
            self.weather.temperature = Float(attributeDict["value"]!)
        }
        else if elementName.lowercased() == "humidity" {
            self.weather.humidity = Float(attributeDict["value"]!)
        }
        else if elementName.lowercased() == "pressure" {
            // Ok, no hacer nada
        }
        else if elementName.lowercased() == "wind" {
            // Ok, no hacer nada
        }
        else if elementName.lowercased() == "speed" {
            self.weather.windSpeed = Float(attributeDict["value"]!)
        }
        else if elementName.lowercased() == "gusts" {
            
        }
        else if elementName.lowercased() == "direction" {
            
        }
        else if elementName.lowercased() == "clouds" {
            
        }
        else if elementName.lowercased() == "visibility" {
            
        }
        else if elementName.lowercased() == "precipitation" {
            
        }
        else if elementName.lowercased() == "weather" {
            self.weather.description = attributeDict["value"]
            let icon = attributeDict["icon"]
            self.weather.imageURL = "http://openweathermap.org/img/w/\(icon!).png"
        }
        else if elementName.lowercased() == "lastupdate" {
            
        }
        else { // Si no puede haber etiquetas distintas
            parser.abortParsing()
        }
    }
    
    func parser(_ parser: XMLParser,
                foundCharacters characters: String)
    {
        // Quitamos espacios en blanco
        let trimmedString = characters.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if trimmedString != "" {
            self.currentMessage = trimmedString
        }
    }

    func parser(_ parser: XMLParser,
                didEndElement elementName: String,
                namespaceURI: String?,
                qualifiedName: String?)
    {
        if elementName.lowercased() == "country" {
            let message = self.currentMessage
            self.weather.country = message
        }
    }
}

