//
//  Connection.swift
//  prueba
//
//  Created by Antonio Pertusa on 13/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import Foundation

protocol ConnectionDelegate {
    func connectionSucceed(_ connection : Connection, with data: Data)
    func connectionFailed(_ connection: Connection, with error: String)
}

class Connection {
    
    var delegate:ConnectionDelegate?
    
    init(delegate : ConnectionDelegate) {
        self.delegate = delegate
    }
    
    func startConnection(_ session: URLSession, with request:URLRequest)
    {
        session.dataTask(with: request, completionHandler: { data, response, error in
            
            if let error = error {
                self.delegate?.connectionFailed(self, with: error.localizedDescription)
            }
            else {
                let res = response as! HTTPURLResponse
                
                if res.statusCode == 200 {
                    DispatchQueue.main.async {
                        self.delegate?.connectionSucceed(self, with: data!)
                    }
                }
                else {
                    let errorMessage=("Received status code: \(res.statusCode)")
                    self.delegate?.connectionFailed(self, with: errorMessage)
                }
            }
        }).resume()
    }
    
    func startConnection(_ session: URLSession, with url:URL)
    {
        self.startConnection(session, with: URLRequest(url:url))
    }
    
    func startConnection(_ session: URLSession, with urlString:String)
    {
        if let encodedString = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL(string: encodedString) {
                self.startConnection(session, with: url)
            }
        }
    }
}

