//
//  Weather.swift
//  weather
//
//  Created by Antonio Pertusa on 19/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import Foundation

class Weather {
    var humidity : Float?
    var temperature : Float?
    var windSpeed : Float?
    var description : String?
    var country : String?
    var imageURL : String?
    
    init?(json: [String: Any]) {
        guard let main = json["main"] as? [String : Float],
            let humidity = main["humidity"],
            let temperature = main["temp"],
            let wind = json["wind"] as? [String : Float],
            let windSpeed = wind["speed"],
            let sys = json["sys"] as? [String : Any],
            let weather = json["weather"] as? [[String: Any]],
            let icon = weather[0]["icon"] as? String,
            let description = weather[0]["description"] as? String,
            let country = sys["country"] as? String
        else{
            return nil
        }
        
        self.humidity = humidity
        self.temperature = temperature
        self.windSpeed = windSpeed
        self.description = description
        self.country = country
        self.imageURL = "http://openweathermap.org/img/w/\(icon).png"
    }
    
    init?() {
        // No hacer nada
    }
}
